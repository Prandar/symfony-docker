<?php

namespace App\Prandar\BundlePrangere\src;

use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class to show what's inside a CSV in a Json format
 */
class ShowCsvJson extends AbstractShowCsv
{
    /**
     * @param OutputInterface $output
     */
    public function renderJson(OutputInterface $output): void
    {
        // format data to be readable
        /** @var $rowsData array */
        $rowsData = $this->formatColumn();

        /** @var $mergedArrayReadable array */
        $mergedArrayReadable = array_merge([AbstractShowCsv::ARRAY_HEADER], $rowsData);

        $output->writeln(json_encode($mergedArrayReadable, JSON_PRETTY_PRINT));
    }
}
