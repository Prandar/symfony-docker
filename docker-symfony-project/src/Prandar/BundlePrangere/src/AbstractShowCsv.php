<?php

namespace App\Prandar\BundlePrangere\src;

/**
 * Abstract class to show what's inside a CSV
 */
abstract class AbstractShowCsv
{
    protected $arrayCsv;

    protected const ACCEPTED_HTML_TAG = ["<br/>", "<br>", "<p>", "<p/>", "\\r"];
    protected const ARRAY_HEADER = ["Sku", "Status", "Price", "Description", "Created at", "Slug"];
    protected const DATE_FORMAT = "l, d-M-y H:i:s T"; // RFC850
    protected const SLUG_DELIMITER = ["_", "-"];

    protected const INDEX_COL_SKU = 0;
    protected const INDEX_COL_IS_ENABLE = 2;
    protected const INDEX_COL_PRICE = 3;
    protected const INDEX_COL_CURRENCY = 4;
    protected const INDEX_COL_DESCRIPTION = 5;
    protected const INDEX_COL_CREATED_AT = 6;
    protected const INDEX_COL_TITLE = 1;

    public function __construct(array $arrayCsv)
    {
        $this->arrayCsv = $arrayCsv;
    }

    /**
     * @return array
     */
    protected function formatColumn(): array
    {
        /** @var $ctr ConvertToReadable */
        $ctr = new ConvertToReadable();

        /** @var $arrayRows array */
        $arrayRows = [];

        for ($row=1; $row < count($this->arrayCsv); $row++) {
            /** @var $arrayRow array */
            $arrayRow = [
                $this->arrayCsv[$row][AbstractShowCsv::INDEX_COL_SKU],
                $ctr->readableStatus($this->arrayCsv[$row][AbstractShowCsv::INDEX_COL_IS_ENABLE]),
                $ctr->readablePrice(
                    $this->arrayCsv[$row][AbstractShowCsv::INDEX_COL_PRICE],
                    $this->arrayCsv[$row][AbstractShowCsv::INDEX_COL_CURRENCY]
                ),
                $ctr->readableDescription(
                    $this->arrayCsv[$row][AbstractShowCsv::INDEX_COL_DESCRIPTION],
                    AbstractShowCsv::ACCEPTED_HTML_TAG
                ),
                $ctr->readableDate(
                    $this->arrayCsv[$row][AbstractShowCsv::INDEX_COL_CREATED_AT],
                    AbstractShowCsv::DATE_FORMAT
                ),
                $ctr->readableSlug(
                    $this->arrayCsv[$row][AbstractShowCsv::INDEX_COL_TITLE],
                    AbstractShowCsv::SLUG_DELIMITER
                )
            ];
            $arrayRows[] = $arrayRow;
        }

        return $arrayRows;
    }
}
