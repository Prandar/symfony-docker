<?php

namespace App\Prandar\BundlePrangere\src;

/**
 * Helper class to format data to be readable
 */
class ConvertToReadable
{
    /**
     * @param string $status
     *
     * @return string
     */
    public function readableStatus(string $status): string
    {
        if ($status === "1")
        {
            return "Enable";
        }

        return "Disable";
    }

    /**
     * @param string $price
     * @param string $currency
     *
     * @return string
     */
    public function readablePrice(string $price, string $currency): string
    {
        /** @var $formattedPrice float */
        $formattedPrice = number_format($price, 1, ',', '');

        return $formattedPrice .'0' .$currency;
    }

    /**
     * @param string $description
     * @param array $acceptedHtmlBalise
     *
     * @return string
     */
    public function readableDescription(string $description, array $acceptedHtmlBalise): string
    {
        /** @var $strippedDescription string */
        $strippedDescription = strip_tags($description, $acceptedHtmlBalise);

        return str_replace($acceptedHtmlBalise, "\n", $strippedDescription);
    }

    public function readableDate(string $dateCreation, string $dateFormat): string
    {
        /** @var $date ?int */
        $date = strtotime($dateCreation);

        // check $date validity
        if ($date !== FALSE) {

            return date($dateFormat, $date);
        }

        return "Invalid Timestamp";
    }

    /**
     * @param string $title
     * @param array $slugDelimiter
     *
     * @return string
     */
    public function readableSlug(string $title, array $slugDelimiter): string
    {

        return strtolower(preg_replace(["/  */", "/[^a-zA-Z0-9]+/"], $slugDelimiter, trim($title)));
    }
}
