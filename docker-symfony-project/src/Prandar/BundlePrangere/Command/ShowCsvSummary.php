<?php
namespace App\Prandar\BundlePrangere\Command;

use App\Prandar\BundlePrangere\src\ShowCsvJson;
use App\Prandar\BundlePrangere\src\ShowCsvTable;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 *
 */
class ShowCsvSummary extends Command
{
    // command name use after "bin/console")
    protected static $defaultName = 'app:show-csv-summary';

    protected $arrayCsv;

    /**
     * @param null $name
     */
    public function __construct($name = null)
    {
        $this->arrayCsv = [];

        parent::__construct($name);
    }

    protected function configure(): void
    {
        $this

        // short description seen after "php bin/console list"
        ->setDescription('Affiche un aperçu d\'un fichier .csv')

        // complete description seen after "--help"
        ->setHelp('Cette commande permet d\'afficher dans un tableau le contenu d\'un fichier .csv')

        // argument csv file path
        ->addArgument('filePath', InputArgument::REQUIRED, 'Chemin du fichier')

        // argument change output format (json ou table)
        ->addArgument('format', InputArgument::OPTIONAL, 'Format du retour (json ou table)')

        ;
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     *
     * @return int
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $inputFilePath = $input->getArgument('filePath');
        $fileParts = pathinfo($inputFilePath);

        // check file existence and extension
        if (file_exists($inputFilePath) && $fileParts['extension'] == 'csv')
        {
            $file = fopen($inputFilePath,"r");

            // get all csv line in an array
            while(!feof($file))
            {
                array_push($this->arrayCsv, fgetcsv($file, 0, ";"));
            }

            fclose($file);

            // check format output
            if ($input->getArgument('format') === "json")
            {
                /** @var $showCsvJson ShowCsvJson */
                $showCsvJson = new ShowCsvJson($this->arrayCsv);
                $showCsvJson->renderJson($output);

                return 0;
            }

            /** @var $showCsvTable ShowCsvTable */
            $showCsvTable = new ShowCsvTable($this->arrayCsv);
            $showCsvTable->renderTable($output);

            return 0;
        }

        $output->writeln('Invalid File or Path');
        return 1;
    }
}
